import {Car} from './car';

export const CARS: Car[] = [
  {
    id: 1,
    mark: 'Saab',
    model: '9-3',
    year: 2003
  },
  {
    id: 2,
    mark: 'Porsche',
    model: '911',
    year: 2012
  },
  {
    id: 3,
    mark: 'Audi',
    model: 'A3',
    year: 2005
  },
  {
    id: 4,
    mark: 'Kia',
    model: 'Seltos',
    year: 2002
  },
  {
    id: 5,
    mark: 'Subaru',
    model: 'Impreza',
    year: 2001
  },
  {
    id: 6,
    mark: 'Audi',
    model: 'A4',
    year: 2015
  },
  {
    id: 7,
    mark: 'Mercedes-Benz',
    model: 'GLC',
    year: 2009
  },
  {
    id: 8,
    mark: 'Porsche',
    model: 'Cayenne',
    year: 2002
  },
  {
    id: 9,
    mark: 'Mazda',
    model: '6',
    year: 2021
  },
  {
    id: 10,
    mark: 'Renault',
    model: 'Arkana',
    year: 2014
  },
  {
    id: 11,
    mark: 'Opel',
    model: 'Vectra',
    year: 2010
  },
  {
    id: 12,
    mark: 'Infiniti',
    model: 'Q50',
    year: 2008
  },
  {
    id: 13,
    mark: 'Dodge',
    model: 'Ram',
    year: 2019
  },
  {
    id: 14,
    mark: 'Hyundai',
    model: 'i30',
    year: 2018
  },
  {
    id: 15,
    mark: 'Aston Martin',
    model: 'Vulcan',
    year: 2003
  },
  {
    id: 16,
    mark: 'Jeep',
    model: 'Wrangler',
    year: 2005
  },
  {
    id: 17,
    mark: 'Alfa Romeo',
    model: '155',
    year: 2011
  }
];
