import{ Component, EventEmitter, Input, Output } from '@angular/core';
import {CARS} from './cars'
import { sort } from 'fast-sort';
import {Car} from './car';

import { TableService } from './table.service';

@Component({
  selector: 'table-component',
  templateUrl:'./table.component.html',
  styleUrls: ['./table.component.sass']
})

export class TableComponent{
  title="List of cars";
  cars;
  sortOrder="";
  sortColumn = "";
  modalTitle: string = "";
  message: string ="message";
  newFilter: string ="";
  active: boolean = false;
  newModel: string = "";
  newModelError: boolean = false;
  newMark: string = "";
  newMarkError: boolean = false;
  newYear: number = 0;
  newYearError: boolean = false;
  newId: number = 0;
  oldId?: number = undefined

  constructor(service: TableService){
    this.cars = service.getCars()
  }

  @Input()
  @Output() voted = new EventEmitter<boolean>();

  setTitle(sortColumn:string){
    this.title=`Sortuj po: ${sortColumn}`
  }

  sortFunction(sortBy:string){
    console.log(this.newFilter)
    const sortAsc = () =>{
      this.cars = sort( CARS.filter(car=>car.model.toLowerCase().includes(this.newFilter))).asc(u=>{
      this.sortColumn = sortBy;
      switch (sortBy){
        case 'mark':
          return u.mark.toLowerCase();
        case 'model':
          return u.model.toLocaleLowerCase();
        case 'year':
          return u.year;
        default:
          return u;
      }
    })
  }

  const sortDesc = () =>{
    this.cars = sort( CARS.filter(car=>car.model.toLowerCase().includes(this.newFilter))).desc(u=>{
    this.sortColumn = sortBy;
    switch (sortBy){
      case 'mark':
        return u.mark.toLowerCase();
      case 'model':
        return u.model.toLocaleLowerCase();
      case 'year':
        return u.year;
      default:
        return u;
    }
  })
}

  if(this.sortColumn === sortBy){
    switch(this.sortOrder){
      case "":
        sortAsc()
        this.sortOrder="asc"
        break
      case "asc":
        sortDesc()
        this.sortOrder="desc"
        break
      default:
        this.cars=CARS
        this.sortOrder=""
        break
      } 
    } else {
      sortAsc()
      this.sortOrder="asc"
    }
  }
  
  deleteFunction(rowId:number){
    this.cars = this.cars.filter(car => car.id != rowId)
  }

  filterFunction(event: any){
    this.newFilter = event.target.value
    this.cars = CARS.filter(car=>car.model.toLowerCase().includes(this.newFilter))
  }

  showModalFunction(title: string, newModel?: string, newMark?:string, newYear?:number, id?:number ){
    this.newModel= newModel ? newModel : ''
    this.newMark = newMark ? newMark : ''
    this.newYear = newYear ? newYear : 0 
    this.newId = id ? id : CARS[CARS.length - 1].id + 1
    this.oldId = id !== undefined ? id : undefined
    this.active = true
    this.modalTitle = title
  }

  closeModalFunction(){
    this.newModelError=false
    this.newMarkError=false
    this.newYearError=false
    this.active=false
  }

  changeModelFunction(event: any){
    this.newModel = event.target.value
  }

  changeMarkFunction(event: any){
    this.newMark = event.target.value
  }

  changeYearFunction(event: any){
    this.newYear = parseInt(event.target.value)
  }

  addFunction(){
    const newCar:Car[] =   [{
      id: this.newId,
      mark: this.newMark,
      model: this.newModel,
      year: this.newYear
    }]

    if (newCar[0].mark !== "" && newCar[0].model !== "" && newCar[0].year > 1900){
      if(this.oldId !== undefined){
        CARS[this.oldId-1] = newCar[0]
        this.active=false
      } else {
        CARS.push(newCar[0])
        this.active=false
      }
    } else {
      this.newMark === "" ? this.newMarkError = true : this.newMarkError = false
      this.newModel === "" ? this.newModelError = true : this.newModelError = false
      this.newYear < 1901 ? this.newYearError = true : this.newYearError = false
    }

    this.cars = CARS
  }
}